const puppeteer = require('puppeteer')

const cratePdf =async ()=>{
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    const options = {
        path: 'pdf/web.pdf',
        format: 'A4'
    };

    await page.goto('https://medium.com/@jsoverson/bypassing-captchas-with-headless-chrome-93f294518337',{waituntil:'networkidle2'});
    await page.pdf(options);
    await browser.close();

}
cratePdf();